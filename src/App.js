import React, {useState} from 'react';
import { sha256 } from 'js-sha256';
import './App.css';

function App() {
  var sha256 = require("js-sha256")
  const [msg, setMsg] = useState('')
  const [hash, setHash] = useState('')

  const convertSHA = () => {
   const hash2 = sha256(msg)
   setHash(hash2)
  }
  
  // const xyz = sha256.update(msg).hex()
  
  // var hash = sha256.create();
  // hash.update('Message to hash');
  // hash.hex();
 
  return (
    <div className="App">
    <div>
    <h1>Welcome to SHA-256 </h1>
    <textarea
     onChange={(e) => setMsg(e.target.value)}
      style={{height:"120px", width:"480px"}} 
      placeholder="Enter any input" 
      value={msg}
      />
    </div>
    <button onClick={convertSHA}>Convert To hash</button>
    <div>
      <p>{hash}</p>
    </div>
    </div>
  );
}

export default App;
